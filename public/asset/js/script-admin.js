jQuery(function($){'use strict',
	$('a[href*=\\#]').on('click', function(e) {
		e.preventDefault();
	});
	$('.div-show-hide-choose-language').on('click', function() {
		var i = $('.div-show-hide-choose-language i');
		if (i.hasClass('fa-eye')) {
			i.removeClass('fa-eye').addClass('fa-eye-slash');
			$('.div-hide').show();
		} else {
			i.addClass('fa-eye').removeClass('fa-eye-slash');
			$('.div-hide').hide();
		}
	});
	$('.czech-republic').on('click', function() {
		$('#czech-republic').toggle();
	});
	$('.germany').on('click', function() {
		$('#germany').toggle();		
	});
	$('.spain').on('click', function() {
		$('#spain').toggle();		
	});
	$('.french').on('click', function() {
		$('#french').toggle();		
	});
	$('.italia').on('click', function() {
		$('#italia').toggle();		
	});
	$('.russia').on('click', function() {
		$('#russia').toggle();		
	});

});