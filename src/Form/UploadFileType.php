<?php

namespace App\Form;

use App\Entity\UploadFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class UploadFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'autocomplete' => 'off',
                ],
                'label' => 'Titre',
                'required' => true
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'attr' => [
                    'class' => 'form-control',
                    'style' => 'height:120px'
                ],
                'required' => false
            ])
            ->add('attachFile', VichFileType::class, [
                'attr' => [
                    'accept'=>'image/*, .txt, .docx, .doc, .pdf, .pptx, .xlsx'
                ],
                'label' => 'Attachement',
                'required' => true,
                'allow_delete' => true,
                'delete_label' => 'Effacher attachement',
                'download_label' => false,
                'asset_helper' => true
            ])
            ->add('status', CheckboxType::class, [
                'label' => 'Je veux ajouter cet attachement.',
                'required' => true 
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UploadFile::class,
        ]);
    }
}
