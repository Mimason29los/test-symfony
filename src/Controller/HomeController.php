<?php

namespace App\Controller;

use App\Entity\UploadFile;
use App\Form\UploadFileType;
use App\Repository\UploadFileRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class HomeController extends AbstractController
{

    /**
     * @Route("/", name="app_home")
     */
    public function index(UploadFileRepository $uploadFileRepository): Response
    {
        $files = $uploadFileRepository->findAll();

        return $this->render('home/index.html.twig', [
            'files' => $files
        ]);
    }

    /**
     * @Route("/add-file", name="app_add_file")
     */
    public function addFile(Request $request, EntityManagerInterface $entityManager): Response
    {
        $file = new UploadFile();

        $form = $this->createForm(UploadFileType::class, $file);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file->setCreatedAt(new \DateTime(('now')));

            $entityManager->persist($file);
            $entityManager->flush();

            $this->addFlash("success", 'Un fichier a bien été ajouté.');
            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('attachment/add_file.html.twig', [
            'form' => $form->createView()
        ]);
    }

}
