### Etape à suivre ###
#1 Faire les étapes suivantes afin de trouver les résultats correctes :

- composer update ou composer install
- changer le mode dev (APP_ENV=dev) en mode prod (APP_ENV=prod) dans le fichier .env afin de
trouver la page d'erreur s'il y a une erreur ou si vous voulez aussi de changer l'URL qui n'existe pas
dans la l'URL.
- effacer le cache : php bin/console cache:clear --no-warmup

#2 Voici le parcours de dossier qui contient les fichiers ajoutés : public\uploads\attachments 

#3 La base de données (test-symfony.sql) se trouve dans ce projet test-symfony.

### Merci ###
